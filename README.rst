========================
node_dendro
========================

.. {# pkglts, doc

.. image:: https://ecosensornet.gitlab.io/node_dendro/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/node_dendro/0.0.1/

.. image:: https://ecosensornet.gitlab.io/node_dendro/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/node_dendro

.. image:: https://ecosensornet.gitlab.io/node_dendro/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://ecosensornet.gitlab.io/node_dendro/

.. image:: https://badge.fury.io/py/node_dendro.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/node_dendro

.. #}
.. {# pkglts, glabpkg_dev, after doc

.. #}

Node for µdendrometer

