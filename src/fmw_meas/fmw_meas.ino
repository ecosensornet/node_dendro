#include <ArduinoJson.h>

#include "src/board.h"
#include "src/calibration/calibration.h"
#include "src/measure/measure.h"

unsigned long t0;

void fmt_meas(String& msg) {
  StaticJsonDocument<128> doc;

  doc["dendro_raw"] = sample_cur.dendro_raw;
  doc["dendro_cal"] = sample_cur.dendro_cal;
  doc["t_box"] = sample_cur.t_box;

  serializeJsonPretty(doc, msg);
}

void setup() {
  board::setup();

  calibration::setup();
  measure::setup();
}

void loop() {
  String msg = "";

  t0 = millis();
  measure::sample();
  Serial.println("elapsed: " + String(millis() - t0));

  fmt_meas(msg);
  Serial.println(msg);

  delay(1000);
}
