"""
Decode message into values
"""
import json
from dataclasses import dataclass


@dataclass
class Sample:
    iid: int = 2
    """Interface id
    """

    dendro_raw: int = 0
    """dendro [#]
    """

    dendro_cal: float = 0
    """dendro [µm]
    """

    t_box: float = 0
    """t2 [°C]
    """

    def decode(self, buffer):
        if buffer[0] != self.iid:
            raise IndexError("bad interface")

        self.dendro_raw = buffer[1] + (buffer[2] << 8)

        dendro_int = buffer[3] + (buffer[4] << 8)
        self.dendro_cal = dendro_int / 3

        t_int = buffer[5] + (buffer[6] << 8)
        self.t_box = t_int / 100 - 273.15

    def to_json(self):
        data = {
            "dendro_raw": self.dendro_raw,
            "dendro_cal": self.dendro_cal,
            "t_box": self.t_box,
        }

        return json.dumps(data, indent=2)
