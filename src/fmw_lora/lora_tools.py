def from_mac(mac):
    deveui = [2, 231]
    for i in range(6):
        deveui.append(int(mac[i * 2:i * 2 + 2], 16))

    return ", ".join(str(v) for v in reversed(deveui))


def for_stack(deveui):
    return "".join(f"{v:02X}" for v in reversed(deveui))
