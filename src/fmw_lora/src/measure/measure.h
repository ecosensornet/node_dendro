#pragma once

#include <Arduino.h>


namespace measure {

typedef struct {
  uint16_t dendro_raw;
  float dendro_cal;
  float t_box;
} Sample;


void setup();
void sample();
}  // namespace measure

extern measure::Sample sample_cur;
