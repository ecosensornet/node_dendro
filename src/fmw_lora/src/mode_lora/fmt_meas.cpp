#include "../settings.h"
#include "../measure/measure.h"

#include "fmt_meas.h"

uint8_t mode_lora::fmt_meas(uint8_t* msg) {
  uint16_t t_int;
  uint16_t dendro_int;

  msg[0] = (uint8_t)IID;

  msg[1] = (uint8_t)(sample_cur.dendro_raw & 0xff);
  msg[2] = (uint8_t)((sample_cur.dendro_raw >> 8) & 0xff);

  dendro_int = (uint16_t)(sample_cur.dendro_cal * 3 + 0.5);
  msg[3] = (uint8_t)(dendro_int & 0xff);
  msg[4] = (uint8_t)((dendro_int >> 8) & 0xff);

  t_int = (uint16_t)((sample_cur.t_box + 273.15) * 100 + 0.5);
  msg[5] = (uint8_t)(t_int & 0xff);
  msg[6] = (uint8_t)((t_int >> 8) & 0xff);

#if (DEBUG == 1)
  Serial.print("cur: (");
  Serial.print(sample_cur.dendro_raw);
  Serial.print(", ");
  Serial.print(sample_cur.dendro_cal);
  Serial.print(", ");
  Serial.print(sample_cur.t_box);
  Serial.println(")");

  Serial.print("[");
  for (uint8_t i = 0; i < 7; i++) {
    Serial.print(msg[i]);
    Serial.print(", ");
  }
  Serial.println("]");
#endif // DEBUG

  return 7;
}

