function decodeUplink(input) {
  if (input.bytes[0] != 2) {
    return {
      data: {},
      warnings: [],
      errors: ["bad node interface"]
    };
  }

  return {
    data: {
      dendro_raw_i: (input.bytes[1]+ (input.bytes[2] << 8)),  // [bytes] to [#]
      dendro_cal_f: (input.bytes[3]+ (input.bytes[4] << 8)) / 3,  // [bytes] to [µm]
      t_box_f: (input.bytes[5]+ (input.bytes[6] << 8)) / 100 - 273.15,  // [bytes] to [°C]
    },
    warnings: [],
    errors: []
  };
}
