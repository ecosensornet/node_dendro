#include <sensor_ds18b20.h>
#include <component_ltc2451.h>
#include <Wire.h>

#include "../settings.h"
#include "../calibration/calibration.h"

#include "measure.h"

LTC2451 adc = LTC2451();
DS18B20 ds(PIN_TH_DATA);
uint8_t ds_address[8];
TwoWire i2c = TwoWire(0);

measure::Sample sample_cur;

void measure::setup() {
  digitalWrite(PIN_CMD_VEXT_SENSOR, LOW);
  delay(500);

  // setup dendro
  i2c.setPins(PIN_I2C_SDA, PIN_I2C_SCL);
  i2c.begin();

  adc.begin(i2c);

  // Check device present
  if (!adc.is_connected()) {
#if (DEBUG == 1)
    Serial.print("No device found at address ");
    Serial.println(adc.address(), HEX);
#endif // DEBUG
    while (true) {
      delay(10);
      board::signal_warning(1);
    }
  }

  // internal temperature sensor
#if (DEBUG == 1)
  Serial.print("Devices: ");
  Serial.print(ds.search());
  Serial.println(" [#]");
#endif // DEBUG
  while (ds.selectNext()) {
    ds.getAddress(ds_address);

#if (DEBUG == 1)
    Serial.print("Address:");
    for (uint8_t i = 0; i < 8; i++) {
      Serial.print(" ");
      Serial.print(ds_address[i]);
    }
    Serial.println();
#endif // DEBUG
  }

   digitalWrite(PIN_CMD_VEXT_SENSOR, HIGH);
}


void measure::sample(){
  uint16_t value;

  digitalWrite(PIN_CMD_VEXT_SENSOR, LOW);
  delay(500);

  adc.convert();
  while (!adc.sample_ready()) {
    delay(100);
  }

  if (adc.read(value)) {
#if (DEBUG == 1)
    Serial.println("Convert error");
#endif // DEBUG
  }

  sample_cur.dendro_raw = value;
  sample_cur.dendro_cal = calib.dendro[0] + calib.dendro[1]  * value + calib.dendro[2]  * value * value;
  yield();

  sample_cur.t_box = ds.getTempC();


   digitalWrite(PIN_CMD_VEXT_SENSOR, HIGH);
}
