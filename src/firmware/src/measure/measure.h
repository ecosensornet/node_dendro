#pragma once

#include <Arduino.h>

#include "../board.h"
#include "measure_base.h"

#define PIN_I2C_SCL PIN_D0
#define PIN_I2C_SDA PIN_D1
#define PIN_TH_DATA PIN_D2

namespace measure {

typedef struct {
  uint16_t dendro_raw;
  float dendro_cal;
  float t_box;
} Sample;

}  // namespace measure

extern measure::Sample sample_cur;
