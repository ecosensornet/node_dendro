#pragma once

#include "settings.h"

namespace node {

void setup();
void loop();

}  // namespace node
