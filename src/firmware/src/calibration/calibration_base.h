#pragma once

#include "../settings.h"


namespace calibration {

void setup();
void load_file();
void save_file();

}  // namespace calibration
