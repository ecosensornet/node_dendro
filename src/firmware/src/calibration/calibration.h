#pragma once

#include "calibration_base.h"


namespace calibration {

typedef struct {
  float dendro[3];
} Calibration;

}  // namespace calibration

extern calibration::Calibration calib;
