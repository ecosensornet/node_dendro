#pragma once

#include "../settings.h"

namespace html_root {

String processor(const String& key);
void handle_html();

void handle_favicon();
void handle_style();
void handle_not_found();

}  // namespace html_root
