#include <Arduino.h>

#include "html_calibration.h"
#include "mode_config.h"


void html_calibration::handle_html() {
#if (DEBUG == 1)
  Serial.println("serving sampling");
#endif
  if (server.method() == HTTP_POST) {
    handle_POST();
  }

  server.process_and_send("/page_calibration.html", html_calibration::processor);
}

