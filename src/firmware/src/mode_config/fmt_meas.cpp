#include <ArduinoJson.h>

#include "../config/config.h"
#include "../measure/measure.h"

#include "fmt_meas.h"


void mode_config::fmt_meas(String& msg) {
  StaticJsonDocument<128> doc;

  doc["dendro_raw"] = sample_cur.dendro_raw;
  doc["dendro_cal"] = sample_cur.dendro_cal;
  doc["t_box"] = sample_cur.t_box;

  serializeJsonPretty(doc, msg);
}

