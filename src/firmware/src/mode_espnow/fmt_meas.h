#pragma once

#include <Arduino.h>

namespace mode_espnow {

uint8_t fmt_meas(uint8_t*);

}  // namespace mode_espnow
