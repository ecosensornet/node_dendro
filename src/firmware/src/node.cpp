#include <Arduino.h>

#include "calibration/calibration.h"
#include "config/config.h"
#include "measure/measure.h"
#include "mode_config/mode_config.h"
#include "mode_espnow/mode_espnow.h"
#include "mode_lora/mode_lora.h"

#include "node.h"


void node::setup() {
#if (DEBUG == 1)
  Serial.println("Setup, mode: " + String(cfg_mode));
#endif // DEBUG
  config::setup();
  calibration::setup();

  switch (cfg_mode)  {
    case MODE_CONFIG:
      mode_config::setup();
      break;
    case MODE_ESPNOW:
      mode_espnow::setup();
      break;
    case MODE_LORA:
      mode_lora::setup();
      break;
    default:
#if (DEBUG == 1)
      Serial.println("userwarning");
      delay(5000);
#endif // DEBUG
      break;
  }

  // measures
  measure::setup();
}


void node::loop() {
  switch (cfg_mode)  {
    case MODE_CONFIG:
      mode_config::loop();
      break;
    case MODE_ESPNOW:
      mode_espnow::loop();
      break;
    case MODE_LORA:
      mode_lora::loop();
      break;
    default:
#if (DEBUG == 1)
      Serial.println("userwarning");
#endif // DEBUG
      break;
  }
}
