#include "../settings.h"
#include "../measure/measure.h"
#include "../mode_espnow/fmt_meas.h"

#include "fmt_meas.h"

uint8_t mode_lora::fmt_meas(uint8_t* msg) {
  return mode_espnow::fmt_meas(msg);
}
